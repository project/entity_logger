<?php

namespace Drupal\entity_logger;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\entity_logger\Entity\EntityLogEntryInterface;

/**
 * Defines an instance class, to log against a specified entity and log channel.
 */
class EntityLoggerInstance implements EntityLoggerInstanceInterface {

  /**
   * The entity logger service.
   *
   * @var \Drupal\entity_logger\EntityLoggerInterface
   */
  protected $entityLogger;

  /**
   * The entity for this entity logger instance.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The optional Drupal logging channel.
   *
   * @var string|null
   */
  protected $channel;

  /**
   * Constructs a EntityLoggerInstance object.
   *
   * @param \Drupal\entity_logger\EntityLoggerInterface $entity_logger
   *   The entity logger service.
   */
  public function __construct(EntityLoggerInterface $entity_logger) {
    $this->entityLogger = $entity_logger;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntity(EntityInterface $entity): EntityLoggerInstanceInterface {
    $this->entity = $entity;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setLoggerChannel(string $channel): EntityLoggerInstanceInterface {
    $this->channel = $channel;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addLogWithSeverity(string $message, array $context = [], int $severity = RfcLogLevel::INFO): ?EntityLogEntryInterface {
    if (!$this->entity) {
      return NULL;
    }
    return $this->entityLogger->log($this->entity, $message, $context, $severity, $this->channel);
  }

  /**
   * {@inheritdoc}
   */
  public function addLog(string $message, array $context = []): ?EntityLogEntryInterface {
    return $this->addLogWithSeverity($message, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function addInfoLog(string $message, array $context = []): ?EntityLogEntryInterface {
    return $this->addLogWithSeverity($message, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function addNoticeLog(string $message, array $context = []): ?EntityLogEntryInterface {
    return $this->addLogWithSeverity($message, $context, RfcLogLevel::NOTICE);
  }

  /**
   * {@inheritdoc}
   */
  public function addWarningLog(string $message, array $context = []): ?EntityLogEntryInterface {
    return $this->addLogWithSeverity($message, $context, RfcLogLevel::WARNING);
  }

  /**
   * {@inheritdoc}
   */
  public function addErrorLog(string $message, array $context = []): ?EntityLogEntryInterface {
    return $this->addLogWithSeverity($message, $context, RfcLogLevel::ERROR);
  }

}
