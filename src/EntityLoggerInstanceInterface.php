<?php

namespace Drupal\entity_logger;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\entity_logger\Entity\EntityLogEntryInterface;

/**
 * Interface for entity_logger.instance service.
 */
interface EntityLoggerInstanceInterface {

  /**
   * Set the entity for this instance.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for this instance.
   *
   * @return \Drupal\entity_logger\EntityLoggerInstanceInterface
   *   The called object.
   */
  public function setEntity(EntityInterface $entity): EntityLoggerInstanceInterface;

  /**
   * Set the optional channel for regular Drupal logging.
   *
   * @param string $channel
   *   The optional channel to use for Drupal logging.
   *
   * @return \Drupal\entity_logger\EntityLoggerInstanceInterface
   *   The called object.
   */
  public function setLoggerChannel(string $channel): EntityLoggerInstanceInterface;

  /**
   * Add log message to entity logger, with given severity.
   *
   * @param string $message
   *   The log message.
   * @param array $context
   *   The log message context.
   * @param int $severity
   *   The severity level.
   *
   * @return \Drupal\entity_logger\Entity\EntityLogEntryInterface|null
   *   The created log entry entity.
   */
  public function addLogWithSeverity(string $message, array $context = [], int $severity = RfcLogLevel::INFO): ?EntityLogEntryInterface;

  /**
   * Add a default log to the entity.
   *
   * @param string $message
   *   The log message.
   * @param array $context
   *   The log message context.
   *
   * @return \Drupal\entity_logger\Entity\EntityLogEntryInterface|null
   *   The created log entry entity.
   */
  public function addLog(string $message, array $context = []): ?EntityLogEntryInterface;

  /**
   * Add an info log to the entity.
   *
   * @param string $message
   *   The log message.
   * @param array $context
   *   The log message context.
   *
   * @return \Drupal\entity_logger\Entity\EntityLogEntryInterface|null
   *   The created log entry entity.
   */
  public function addInfoLog(string $message, array $context = []): ?EntityLogEntryInterface;

  /**
   * Add a notice log to the entity.
   *
   * @param string $message
   *   The log message.
   * @param array $context
   *   The log message context.
   *
   * @return \Drupal\entity_logger\Entity\EntityLogEntryInterface|null
   *   The created log entry entity.
   */
  public function addNoticeLog(string $message, array $context = []): ?EntityLogEntryInterface;

  /**
   * Add a warning log to the entity.
   *
   * @param string $message
   *   The log message.
   * @param array $context
   *   The log message context.
   *
   * @return \Drupal\entity_logger\Entity\EntityLogEntryInterface|null
   *   The created log entry entity.
   */
  public function addWarningLog(string $message, array $context = []): ?EntityLogEntryInterface;

  /**
   * Add an error log to the entity.
   *
   * @param string $message
   *   The log message.
   * @param array $context
   *   The log message context.
   *
   * @return \Drupal\entity_logger\Entity\EntityLogEntryInterface|null
   *   The created log entry entity.
   */
  public function addErrorLog(string $message, array $context = []): ?EntityLogEntryInterface;

}
