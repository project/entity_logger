<?php

namespace Drupal\entity_logger;

use Drupal\Core\Entity\EntityInterface;

/**
 * Factory class for entity logger instances.
 */
class EntityLoggerInstanceFactory implements EntityLoggerInstanceFactoryInterface {

  /**
   * The entity logger instance service.
   *
   * @var \Drupal\entity_logger\EntityLoggerInstanceInterface
   */
  protected $entityLoggerInstance;

  /**
   * Constructs a new EntityLogger Instance object.
   *
   * @param \Drupal\entity_logger\EntityLoggerInstanceInterface $entity_logger_instance
   *   The entity logger instance service.
   */
  public function __construct(EntityLoggerInstanceInterface $entity_logger_instance) {
    $this->entityLoggerInstance = $entity_logger_instance;
  }

  /**
   * {@inheritdoc}
   */
  public function get(EntityInterface $entity, string $channel = NULL): EntityLoggerInstance {
    $this->entityLoggerInstance->setEntity($entity);
    if ($channel) {
      $this->entityLoggerInstance->setLoggerChannel($channel);
    }
    return $this->entityLoggerInstance;
  }

}
