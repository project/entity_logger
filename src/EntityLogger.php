<?php

namespace Drupal\entity_logger;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\entity_logger\Entity\EntityLogEntryInterface;

/**
 * Service for logging to entities.
 */
class EntityLogger implements EntityLoggerInterface {

  /**
   * The entity_log_entry entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityLogEntryStorage;

  /**
   * The entity_logger module settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $entityLoggerSettings;

  /**
   * Factory service to load default Drupal logging service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The message's placeholders parser.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  protected $parser;

  /**
   * EntityLogger constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   The parser to use when extracting message variables.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory, LogMessageParserInterface $parser) {
    $this->entityLogEntryStorage = $entity_type_manager->getStorage('entity_log_entry');
    $this->entityLoggerSettings = $config_factory->get('entity_logger.settings');
    $this->loggerFactory = $logger_factory;
    $this->parser = $parser;
  }

  /**
   * {@inheritdoc}
   */
  public function log(EntityInterface $entity, string $message, array $context = [], int $severity = RfcLogLevel::INFO, string $logger_channel = NULL): ?EntityLogEntryInterface {
    $enabled_entity_types = $this->entityLoggerSettings->get('enabled_entity_types');
    if (!in_array($entity->getEntityTypeId(), $enabled_entity_types)) {
      return NULL;
    }

    // Convert PSR3-style messages to \Drupal\Component\Render\FormattableMarkup
    // style, so they can be translated too in runtime.
    $context = $this->parser->parseMessagePlaceholders($message, $context);

    /** @var \Drupal\entity_logger\Entity\EntityLogEntryInterface $log_entry */
    $log_entry = $this->entityLogEntryStorage->create([]);
    $log_entry->setTargetEntity($entity);
    $log_entry->setMessage($message, $context);
    $log_entry->setSeverity($severity);
    $log_entry->save();

    if ($logger_channel) {
      $this->loggerFactory->get($logger_channel)->log($severity, $message, $context);
    }
    return $log_entry;
  }

}
