<?php

namespace Drupal\entity_logger;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface for entity_logger.instance.factory service.
 */
interface EntityLoggerInstanceFactoryInterface {

  /**
   * Get an entity logger instance.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to log to.
   * @param string|null $channel
   *   The optional Drupal log channel to log to.
   *
   * @return \Drupal\entity_logger\EntityLoggerInstance
   *   The entity logger instance.
   */
  public function get(EntityInterface $entity, string $channel = NULL): EntityLoggerInstance;

}
